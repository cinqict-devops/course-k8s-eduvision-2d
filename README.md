
# Kubernetes + AKS course (2 days)

## Main overview

- Day 1: Kubernetes core
- Day 2: Kubernetes+ and AKS

## Main Goal

The basics of Kubernetes is a steep learning path. This course is ment to guide you through this first part, learn the basics and learn it right. The goal is to enable you to make the next steps yourself.

## Labs

You learn Kubernetes by doing. Each day consist of multiple labs where you learn hands on.

## Prerequisites

Either:

- SSH client (e.g. Putty, or ssh command line tool) 

or some tools (prefered)

- `git`
- `kubectl`

Having some tools locally installed has preference because:

- you can apply what you have learned in your work more easily.
- you can use your favourite editor to edit Dockerfiles and k8s yaml's.

If you can't install these tools locally you can use the provided VM's to work on, which you can reach via a ssh client.

## Infra

The following will be provided:

- Ubuntu VM's containing all commandline tools you need
- Kubernetes clusters (AKS)

## Agenda

- Why Container Orchestrator
- What is Kubernetes
- Why Kubernetes
- How to use
  - Kubectl
  - Pods
  - Deployment
  - Services
  - Namespaces
  - ConfigMaps, Volumes
  - Troubleshooting
- Tips & Tricks
- What & Why Kubernetes Cloud Service (AKS/EKS/GKE)
- K8s Life Cycle Management
- How to use: K8s + AKS
- Services + AKS
- What did we not touch?
- CNCF ecosystem
