
# Ingress Controller

Installatie van de `ingress-nginx` ingress controller met een Azure load balancer met een interne IP.

[Helm config file](./ingress-controller-values.yaml)

```shell
# Create admin namespace
kubectl create namespace k8s-admin

# Install ingress controller
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace k8s-admin

# Verify
kubectl --namespace k8s-admin get pods
kubectl --namespace k8s-admin get services

```

Let op: Het kan enkele minuten duren voordat de Azure load balancer is aangemaakt en de `external` IP zichtbaar wordt.

Verifieer dat nginx draait door te browsen naar: http://EXTERNAL_IP

Om het mooi te maken zou je via je lokale `/etc/hosts` file het EXTERNAL_IP een DNS naam kunnen geven

```shell
example.com EXTERNAL_IP
```

## Verify ingress

```shell
kubectl create namespace test-ingress

kubectl --namespace test-ingress apply -f ./test-ingress

kubectl --namespace test-ingress get pods
kubectl --namespace test-ingress get services
kubectl --namespace test-ingress get ingress

```

Als DNS niet werkt kan je de pods zo bereiken:

```shell
curl -H "Host: demo.example.com" http://EXTERNAL_IP
curl -H "Host: helloworld.example.com" http://EXTERNAL_IP
curl -H "Host: helloworld.demo.example.com" http://EXTERNAL_IP
```

Als DNS werkt kan je de pods zo bereiken:

```shell
curl http://demo.example.com
curl http://helloworld.example.com
curl http://helloworld.demo.example.com
```

## Bronnen

- https://docs.microsoft.com/en-us/azure/aks/ingress-internal-ip
- https://kubernetes.io/docs/concepts/services-networking/ingress/#name-based-virtual-hosting